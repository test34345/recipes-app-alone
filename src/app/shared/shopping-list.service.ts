import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import {
  ShoppingList,
  ShoppingListItem,
} from '../shopping-list/shopping-list.model';

const shoppingListInitialValue: ShoppingList = {
  items: [],
};

@Injectable({
  providedIn: 'root',
})
export class ShoppingListService {
  shoppingList: ShoppingList = shoppingListInitialValue;
  shoppingListChanged = new Subject<ShoppingList>();

  private notifyShoppingListChanged() {
    this.shoppingListChanged.next({ ...this.shoppingList });
  }

  getShoppingList() {
    return { ...this.shoppingList };
  }

  addItem(newItem: ShoppingListItem) {
    this.shoppingList.items.push(newItem);
    this.notifyShoppingListChanged();
  }

  removeItem(indexOfItem: number) {
    this.shoppingList.items.splice(indexOfItem, 1);
    this.notifyShoppingListChanged();
  }
}
