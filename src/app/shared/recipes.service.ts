import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Recipe } from '../recipes/recipe.model';

const initialRecipes: Recipe[] = [
  new Recipe(
    'Recipe 1',
    'https://images.unsplash.com/photo-1542010589005-d1eacc3918f2',
    'Some description'
  ),
];

@Injectable({
  providedIn: 'root',
})
export class RecipesService {
  private recipes: Recipe[] = initialRecipes;
  recipesChanged = new Subject<Recipe[]>();

  private notifySubscribersOnRecipesChanged() {
    this.recipesChanged.next([...this.recipes]);
  }

  getRecipes() {
    return [...this.recipes];
  }

  addRecipe(newRecipe: Recipe) {
    this.recipes.push(newRecipe);
    this.notifySubscribersOnRecipesChanged();
  }

  removeRecipe(recipeIndex: number) {
    this.recipes.splice(recipeIndex, 1);
    this.notifySubscribersOnRecipesChanged();
  }
}
