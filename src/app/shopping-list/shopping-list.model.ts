export class ShoppingListItem {
  constructor(public title: string, public amount: number) {}
}

export type ShoppingList = {
  items: ShoppingListItem[];
};
