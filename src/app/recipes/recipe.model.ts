export class Recipe {
  constructor(
    public title: string,
    public imageUrl: string,
    public description: string
  ) {}
}
