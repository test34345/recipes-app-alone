import { NgModule } from '@angular/core';
import { RecipesPageComponent } from './recipes-page/recipes-page.component';

@NgModule({
  declarations: [RecipesPageComponent],
  exports: [RecipesPageComponent],
})
export class PagesModule {}
