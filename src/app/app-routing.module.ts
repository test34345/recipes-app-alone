import { NgModule } from '@angular/core';
import { RouterModule, Route } from '@angular/router';
import { RecipesPageComponent } from './pages/recipes-page/recipes-page.component';
import { NewRecipePageComponent } from './pages/new-recipe-page/new-recipe-page.component';
import { ShoppingListPage } from './pages/shopping-list-page/shopping-list-page.component';
import { NotFoundPage } from './pages/not-found-page/not-found-page.component';
import { LoginPage } from './pages/login-page/login-page.component';

const appRoutes: Route[] = [
  { path: '', redirectTo: 'recipes/all', pathMatch: 'full' },
  { path: 'recipes/all', component: RecipesPageComponent },
  { path: 'recipes/new', component: NewRecipePageComponent },
  { path: 'shopping-list', component: ShoppingListPage },
  { path: 'login', component: LoginPage },
  { path: '**', component: NotFoundPage },
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
